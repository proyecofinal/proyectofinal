<?php

namespace app\controllers;

use Yii;
use app\models\Compran;
use app\models\Entradas;
use app\models\Clientes;
use app\models\Festivales;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Dompdf\Dompdf;


/**
 * CompranController implements the CRUD actions for Compran model.
 */
class CompranController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionNombre()
    {
        return $this->render('cambio_nombre');
    }

    /**
     * Lists all Compran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Compran::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compran model.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_clientes, $cod_entradas, $cod_festivales)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_clientes, $cod_entradas, $cod_festivales),
        ]);
    }

    /**
     * Creates a new Compran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Compran();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Compran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_clientes, $cod_entradas, $cod_festivales)
    {
        $model = $this->findModel($cod_clientes, $cod_entradas, $cod_festivales);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Compran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_clientes, $cod_entradas, $cod_festivales)
    {
        $this->findModel($cod_clientes, $cod_entradas, $cod_festivales)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Compran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cod_clientes
     * @param integer $cod_entradas
     * @param integer $cod_festivales
     * @return Compran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_clientes, $cod_entradas, $cod_festivales)
    {
        if (($model = Compran::findOne(['cod_clientes' => $cod_clientes, 'cod_entradas' => $cod_entradas, 'cod_festivales' => $cod_festivales])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionImprimir($cod_clientes, $cod_entradas, $cod_festivales)
    {
        $cesion = Compran::find($cod_clientes); 
        $pdf = new Dompdf();
        // ob_start();
        // include_once 'datos.php';
        // $plantilla = ob_get_clean();
        $file_name = 'datos_cliente';
        
        $pdf->loadHTML('hola mundo');
        $pdf->render();
        return $pdf->stream($file_name. '.pdf');
    }

    public function actionCambioNombre($cod_clientes)
    {
        $model = $this->findModel($cod_clientes);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['cambio_nombre', 'cod_clientes' => $model->cod_clientes]);
        }

        return $this->render('cambio_nombre', [
            'model' => $model,
        ]);
    }
}
