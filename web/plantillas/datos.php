<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->nombre." " .$model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clientes-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod',
            'dni',
            'nombre',
            'apellidos',
            'edad',
            'fecha_nacimiento',
        ],
    ]) ?>

    

</div>