<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Festivales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-fests">
    <div class="col-md-12">
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail">
                <div class="caption">
                    <h3 class="text-uppercase">Medusa</h3>
                    <p>El Medusa Sunbeach Festival es un festival de música electrónica dirigido principalmente
                        hacia un público joven de 16 años hasta la sepultura. Se celebra desde 2014 cada año <strong>en
                            la playa de Cullera, municipio de Valencia, Comunidad Valenciana.</strong><p>
                    <div>
                        <?= Html::a('Más Información', ['/festivales/medusa'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 mt"></div>
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail">
                <div class="caption">
                    <h3 class="text-uppercase">Arenal Sound</h3>
                    <p>El festival Arenal Sound es un festival de música independiente que <strong>se celebra en la playa El Arenal, 
                            en la localidad de Burriana</strong>, durante la primera semana de agosto desde 2010.</p>
                    <div>
                        <?= Html::a('Más Información', ['/festivales/arenal'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 mt" >
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail ">
                <div class="caption">
                    <h3 class="text-uppercase">Riverland</h3>
                    <p>El festival Riverland es un festival de música independiente <strong>celebrado en la localidad de Arriondas 
                            (provincia de Asturias, España)</strong> e inaugurado la primera semana de agosto de 2019.</p>
                    <div>
                        <?= Html::a('Más Información', ['/festivales/riverland'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 mt"></div>
        <div class="col-sm-5 text-center bordes bg-cuadrado">
            <div class="thumbnail">
                <div class="caption">
                    <h3 class="text-uppercase">Sunblast</h3>
                    <p>El mayor festival de Canarias celebrado en Costa Adeje</p>
                    <div>
                        <?= Html::a('Más Información', ['/festivales/sunblast'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
		
    
</div>

