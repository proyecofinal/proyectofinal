<?php
/* @var $this yii\web\View */

use yii\helpers\Html;


$this->title = 'Entradas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-entradas">
    <div class="col-md-12">
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta para sacar nombre</h3>
                    <p>Total</p>
                    <div>
                        <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 mt"></div>
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta para sacar nombre</h3>
                    <p>Consulta para E.Restantes/E.Totales</p>
                    <div>
                        <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 mt">
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta para sacar nombre</h3>
                    <p>Consulta para E.Restantes/E.Totales</p>
                    <div>
                        <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2 mt"></div>
        <div class="col-sm-5 text-center bordes bg-imagen">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta para sacar nombre</h3>
                    <p>Consulta para E.Restantes/E.Totales</p>
                    <div>
                        <?= Html::a('Comprar', ['/compran/create'], ['class' => 'btn btn-warning']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

