<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clientes */

$this->title = $model->nombre." " .$model->apellidos;
$this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clientes-view">

    <h1>Su código para la compra de entradas es: <?= Html::encode($model->cod) ?></h1>

    <p>
        Si desea cambiar algun dato de la siguiente tablal pulse <?= Html::a('aquí', ['update', 'id' => $model->cod]) ?>
    </p>

    <?= $plantilla = DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod',
            'dni',
            'nombre',
            'apellidos',
            'edad',
            'fecha_nacimiento',
        ],
    ]) ?>

    <?= Html::a('Imprimir Datos', ['imprimir', 'id' => $model->cod], ['class' => 'btn btn-warning']) ?>

</div>
