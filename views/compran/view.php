<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = $model->cod_clientes;
$this->params['breadcrumbs'][] = ['label' => 'Comprans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="compran-view">

    <h1>Código: <?= Html::encode($this->title) ?></h1>
    <p>
        Este código se le pedira si desea realizar un cambio de nombre de nombre
    </p>

    <?= DetailView::widget([
        'model' => $model,
        

        'attributes' => [
            [                                                  
                'label' => 'Nombre del Cliente',
                'value' => $model->codClientes->nombre,
                 
            ],
            [                                                  
                'label' => 'Numero de Entrada',
                'value' => $model->codEntradas->numero_entrada,
                 
            ],
            [                                                  
                'label' => 'Nombre del  Festival',
                'value' => $model->codFestivales->nombre,
                 
            ],
        ],
    ]) ?>

    <?= Html::a('Imprimir Entrada', ['imprimir', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales], ['class' => 'btn btn-warning']) ?>
</div>