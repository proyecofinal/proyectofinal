<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compran-form">

    <p>
        Introduce el código que se le dio al comprar la entrada
    </p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_clientes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('cambiarNombre', ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
