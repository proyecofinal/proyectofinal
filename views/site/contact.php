<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacto';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact"  >
    <div class="col-md-12 bg-imagen" style="padding-top: 10px; padding-bottom: 10px;" >
        <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>

<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Thank you for contacting us. We will respond to you as soon as possible.
            </div>

            <p>
                Note that if you turn on the Yii debugger, you should be able
                to view the mail message on the mail panel of the debugger.
    <?php if (Yii::$app->mailer->useFileTransport): ?>
                    Because the application is in development mode, the email is not sent but saved as
                    a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                    Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                    application component to be false to enable email sending.
    <?php endif; ?>
            </p>

<?php else: ?>
            <!-- <div class="col-md-1"></div>-->
            <div class="col-md-5 pt" style="margin-bottom: 10px;">
                <p>
                    <strong>Proyecto Final</strong>
                </p>
                <p>C/Vargas 65<br>
                    39010 Santander, Cantabria
                </p>
                <p>
                    <a href="tel:+34-942-23-13-44"><i class="fas fa-phone"></i> 942 23 13 44</a>
                    <!--<?= Html::a('<i class="fas fa-phone"></i> 942 23 13 44', '+34-942-23-13-44', ['target' => '_blank']) ?>--><br>
    <?= Html::mailto('<i class="fas fa-envelope"></i> ceinmark@ceinmark.net', 'ceinmark@ceinmark.net', ['target' => '_blank']) ?><br>
                </p>


                <p>
                    If you have questions, please fill out the following form to contact us.
                    Thank you.
                </p>

    <?= Html::img('@web/imagenes/logo.png', ['alt' => 'Logo dla empresa', 'style' => 'margin-left: 120px; margin-right: auto; margin-top: 25px;  width: 200px; height: 200px;']); ?>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">

    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

                <?=
                $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-md-3">{image}</div><div class="col-md-6">{input}</div></div>',
                ])
                ?>

                <div class="form-group flex-row d-flex" style="justify-content: center;" >
                    <?= Html::submitButton('Enviar', ['class' => 'btn btn-warning', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-md-12" style="padding: 0px;">
            <h2>UBICACIÓN</h2>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2896.0857245777697!2d-3.8262577851268182!3d43.45880747299819!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd494bd54e2b75e9%3A0xa5a41632017b71a0!2sCeinmark!5e0!3m2!1ses!2ses!4v1613639819891!5m2!1ses!2ses" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    <?php endif; ?>
</div>
